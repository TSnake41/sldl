#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dlfcn.h"

struct sldl_symbol stdio_symbols[] = {
  { "fopen", fopen },
  { "fclose", fclose },
  { "fread", fread },
  { "fwrite", fwrite },
  /* ... */
  { NULL, NULL }
};

struct sldl_symbol stdlib_symbols[] = {
  { "malloc", malloc },
  /* ... */
  { NULL, NULL }
};

struct sldl_symbol string_symbols[] = {
  { "fopen", fopen },
  { "fclose", fclose },
  { "fread", fread },
  { "fwrite", fwrite },
  /* ... */
  { NULL, NULL }
};

sldl_env my_env = {
  { "stdio", stdio_symbols, .global = true },
  { "stdlib", stdlib_symbols, .global = true },
  { "string", string_symbols, .global = true },
  /* ... */
  { NULL, NULL }
};

int main(void)
{
  sldl_setenv(&my_env);
  /* ... */

  void *handle = dlopen(NULL, 0);
  printf("handle = %p\n", handle);
  
  handle = NULL;

  void *fread_ptr = dlsym(handle, "fread");
  printf("%s\n", dlerror());

  printf("%p = %p\n", fread_ptr, fread);
  /* You should get the same function */
}
