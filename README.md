sldl, in-memory static linking based dlfcn implementation for embedded platforms.

# Why another libdl ?

In many case, you expect to use libraries that suprisingly depends on libdl.
However, in some situations, libdl is unavailable (and therefore, you don't have
it). So it leads to issues like missing features or even impossibility to build.
This is a case in embedded platforms (e.g game consoles or even ardiunos) or with
static linking (where libdl is "disabled") or maybe some obscure platforms that
does not support dynamic linking (e.g MS-DOS).

Some projects that are candidates to use it are mostly projects that heavily
use LuaJIT and needs it's FFI, for example TurtleP's LovePotion or Luvi (with
new uv-ffi).

# Architecture

To resolve this issues, we need to implement a lightweight, in-memory as there
is no way to deal with dynamic linking and native code loading.
So I decided to create a design based on a "environment" that contains all the
named modules that can be loaded with sldl_dlopen and these named modules
contains a symbol set that is used with dlsym. The two remaining functions are
very basic (error management and dlclose that is a empty function (there is
no dynamic allocation)).

So, sldl can only deal with already "loaded" symbols by giving away function
pointers with a libdl-like API.

# How to use ?

First, you can either use sldl.h single-file header library that is a C99 standard
implementation (a C89 or C++ port should very straightforward to make) or you can
also use dlfcn.h and dlfcn.c (and eventually build a libdl.a) to get a libdl
API.

To use it, you need to create a `struct sldl_env` environment, this environment will
contain all modules and symbols need.

This environment should be constant and must be persistent (must not be moved
or freed), otherwise, all your handles became invalid and you will have to
redefine to a valid environment !

Example with some standard functions :
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dlfcn.h"

struct sldl_symbol stdio_symbols[] = {
  { "fopen", fopen },
  { "fclose", fclose },
  { "fread", fread },
  { "fwrite", fwrite },
  /* ... */
  { NULL, NULL }
};

struct sldl_symbol stdlib_symbols[] = {
  { "malloc", malloc },
  /* ... */
  { NULL, NULL }
};

struct sldl_symbol string_symbols[] = {
  { "fopen", fopen },
  { "fclose", fclose },
  { "fread", fread },
  { "fwrite", fwrite },
  /* ... */
  { NULL, NULL }
};

sldl_env my_env = {
  { "stdio", stdio_symbols },
  { "stdlib", stdlib_symbols },
  { "string", string_symbols },
  /* ... */
  { NULL, NULL }
};

int main(void)
{
  sldl_setenv(&my_env);
  /* ... */

  void *handle = dlopen("stdio", 0);
  printf("handle = %p\n", handle);

  void *fread_ptr = dlsym(handle, "fread");
  printf("%s\n", dlerror());

  printf("%p = %p\n", fread_ptr, fread);
  /* You should get the same function */
}
```

# Supported features and limitations

Feature set is very limited currently, there is no support of dlopen with GLOBAL
flag (the flag is ignored). The global flag is setted up in modules while making
the environment with the `sldl_module.global` for each modules.

Outside that, all dlopen flags are ignored as there is no notion of lazy/late
binding as there is no dynamic loading.

# Copying

```
Copyright (C) 2019 Teddy ASTIE

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
